// Modal

// Abrimos la ventana modal.
$(".producto").on("click", function() {
  $(".producto-modal").addClass("active");
  document.body.style.overflowY = "hidden"; // Inhabilita scroll del body
});

// Cerramos la ventana modal.
$('#close-modal').click(function() {
    event.preventDefault();
    $('.producto-modal').removeClass('active');
    document.body.style.overflowY = "scroll";
});

// Presionando la tecla escape también cerramos la ventana modal.
$(document).keyup(function(e) {
  if (e.keyCode === 27) {
    $(".producto-modal").removeClass("active");
    document.body.style.overflowY = "scroll";
  }
});

// Clickeando fuera del modal también lo cerramos.
$(document).click(function(event) {
  if (!$(event.target).closest(".producto,.active").length) {
    $("body")
      .find(".producto-modal")
      .removeClass("active");
    document.body.style.overflowY = "scroll";
  }
});